﻿Public Class Cliente
    Private Sub BRegresar_Click(sender As Object, e As EventArgs) Handles BRegresar.Click
        Dim RegresarMenuPrincipal = New MenuPrincipal
        RegresarMenuPrincipal.Show()
        Me.Hide()

    End Sub

    Private Sub IrInsertar_Click(sender As Object, e As EventArgs) Handles IrInsertar.Click
        Dim IrIngresarCliente = New IngresarCliente
        IrIngresarCliente.Show()
        Me.Hide()
    End Sub

    Private Sub IrModificar_Click(sender As Object, e As EventArgs) Handles IrModificar.Click
        Dim IrModicarCliente = New ModificarCliente
        IrModicarCliente.Show()
        Me.Hide()

    End Sub

    Private Sub IrEliminar_Click(sender As Object, e As EventArgs) Handles IrEliminar.Click
        Dim IrEliminarCliente = New EliminarCliente
        IrEliminarCliente.Show()
        Me.Hide()

    End Sub
End Class