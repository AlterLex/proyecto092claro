﻿
Imports System.Data.OleDb
Public Class IngresarCliente
    Private Sub Regresar_Click(sender As Object, e As EventArgs) Handles Regresar.Click
        Dim regreso = New Cliente
        regreso.Show()
        Me.Hide()
    End Sub

    Private Sub Ingresar_Click(sender As Object, e As EventArgs) Handles Ingresar.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "insert into [Cliente]([Nombre],[Apellido],[Direccion],[Dpi],[FechaNacimiento]) values (@nom,@ape,@dir,@dpi,@fe) "
        Dim Comando As New OleDbCommand(Cadena, conx)
        Comando.Parameters.AddWithValue("@nom", TxtNombre.Text)
        Comando.Parameters.AddWithValue("@ape", TxtApellido.Text)
        Comando.Parameters.AddWithValue("@dir", TxtDireccion.Text)
        Comando.Parameters.AddWithValue("@dpi", TxtDpi.Text)
        Comando.Parameters.AddWithValue("@fe", MonthCalendar1.SelectionRange.Start)
        Dim i As Int32 = Comando.ExecuteNonQuery()
        If i > 0 Then
            MessageBox.Show("Insertado con exito")
            Limpliar()
        Else
            MessageBox.Show("No se pudo Insertar")
        End If
        conx.Close()
    End Sub

    Private Sub Limpliar()
        TxtNombre.Text = ""
        TxtApellido.Text = ""
        TxtDireccion.Text = ""
        TxtDpi.Text = ""
        MonthCalendar1.SetDate(MonthCalendar1.TodayDate)
    End Sub

End Class