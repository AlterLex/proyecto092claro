﻿Imports System.Data.OleDb

Public Class ModificarServicio
    Private Sub BRegresar_Click(sender As Object, e As EventArgs) Handles BRegresar.Click
        Dim regreso = New Servicio
        regreso.Show()
        Me.Hide()

    End Sub

    Private Sub ModificarServicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Servicio]"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Servicios")
        ComboServicio.DataSource = Data.Tables(0)
        ComboServicio.DisplayMember = Data.Tables(0).Columns(0).Caption.ToString()
        ComboServicio.ValueMember = Data.Tables(0).Columns(1).Caption.ToString()
        Dim ComboSelecionado As String = ComboServicio.SelectedValue.ToString
        TxtServicio.Text = ComboSelecionado
        conx.Close()
    End Sub

    Private Sub ComboServicio_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboServicio.SelectedIndexChanged
        Dim ComboSelecionado As String = ComboServicio.SelectedValue.ToString
        TxtServicio.Text = ComboSelecionado
    End Sub

    Private Sub BModificar_Click(sender As Object, e As EventArgs) Handles BModificar.Click
        Dim Desicion As Integer = MessageBox.Show("Desea modificar Servicio", "Modificar Servicio", MessageBoxButtons.YesNo)

        If Desicion = DialogResult.Yes Then
            Dim conx As OleDbConnection = Conexion.ConexionBD
            Dim Cadena As String = "Update [Servicio] set [Descripcion]=@texto where [CodigoServicio]=@numero "
            Dim Comando As New OleDbCommand(Cadena, conx)
            Comando.Parameters.AddWithValue("@texto", TxtServicio.Text)
            Comando.Parameters.AddWithValue("@numero", ComboServicio.Text)
            Dim i As Int32 = Comando.ExecuteNonQuery()
            If i > 0 Then
                MessageBox.Show("Modificado con exito")
            Else
                MessageBox.Show("No se pudo Modificar")
            End If
            Dim Cadena1 As String = "Select * from [Servicio]"
            Dim Adaptador = New OleDbDataAdapter(Cadena1, conx)
            Dim Data As New DataSet
            Adaptador.Fill(Data, "Servicios")
            ComboServicio.DataSource = Data.Tables(0)
            ComboServicio.DisplayMember = Data.Tables(0).Columns(0).Caption.ToString()
            ComboServicio.ValueMember = Data.Tables(0).Columns(1).Caption.ToString()
            Dim ComboSelecionado As String = ComboServicio.SelectedValue.ToString
            TxtServicio.Text = ComboSelecionado
            conx.Close()
        End If
    End Sub
End Class