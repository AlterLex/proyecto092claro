﻿Imports System.Data.OleDb


Public Class ModificarCliente
    Private Sub Regresar_Click(sender As Object, e As EventArgs) Handles Regresar.Click
        Dim Regreso = New Cliente
        Regreso.Show()
        Me.Hide()
    End Sub

    Private Sub BBuscar_Click(sender As Object, e As EventArgs) Handles BBuscar.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Cliente] where [CodigoCliente]=@cod"
        Dim comando = New OleDbCommand(Cadena, conx)
        comando.Parameters.AddWithValue("@cod", TxtCodigo.Text)
        Dim Adaptador = New OleDbDataAdapter()
        Adaptador.SelectCommand = comando
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Clientes")
        If ((Data.Tables.Count > 0) AndAlso (Data.Tables(0).Rows.Count > 0)) Then
            TxtNombre.Enabled = True
            TxtApellido.Enabled = True
            TxtDireccion.Enabled = True
            TxtDpi.Enabled = True
            MonthCalendar1.Enabled = True
            TxtCodigo.Enabled = False
            BBuscar.Enabled = False
            BModificar.Enabled = True

            TxtNombre.Text = Data.Tables(0).Rows(0).Item(1).ToString()
            TxtApellido.Text = Data.Tables(0).Rows(0).Item(2).ToString()
            Dim fecha As Date = Date.Parse(Data.Tables(0).Rows(0).Item(3).ToString())
            MonthCalendar1.SetDate(fecha)
            TxtDireccion.Text = Data.Tables(0).Rows(0).Item(4).ToString()
            TxtDpi.Text = Data.Tables(0).Rows(0).Item(5).ToString()

        Else
            MessageBox.Show("No existe Cliente con ese codigo")
        End If
        conx.Close()
    End Sub

    Private Sub BModificar_Click(sender As Object, e As EventArgs) Handles BModificar.Click

        Dim Desicion As Integer = MessageBox.Show("Desea modificar cliente", "Modificar Cliente", MessageBoxButtons.YesNo)

        If Desicion = DialogResult.Yes Then
            Dim conx As OleDbConnection = Conexion.ConexionBD
            Dim Cadena As String = "Update [Cliente] set [Nombre]=@nom,[Apellido]=@ape,[FechaNacimiento]=@fe,[Direccion]=@dir, [Dpi]=@dpi where [CodigoCliente]=@numero "
            Dim Comando As New OleDbCommand(Cadena, conx)

            Comando.Parameters.AddWithValue("@nom", TxtNombre.Text)
            Comando.Parameters.AddWithValue("@ape", TxtApellido.Text)
            Comando.Parameters.AddWithValue("@fe", MonthCalendar1.SelectionRange.Start)
            Comando.Parameters.AddWithValue("@dir", TxtDireccion.Text)
            Comando.Parameters.AddWithValue("@dpi", TxtDpi.Text)
            Comando.Parameters.AddWithValue("@numero", TxtCodigo.Text)



            Dim i As Integer = Comando.ExecuteNonQuery()


            If i > 0 Then
                MessageBox.Show("Modificado con exito")
                TxtNombre.Enabled = False
                TxtApellido.Enabled = False
                TxtDireccion.Enabled = False
                TxtDpi.Enabled = False
                MonthCalendar1.Enabled = False
                TxtCodigo.Enabled = True
                BBuscar.Enabled = True
                BModificar.Enabled = False
                Limpliar()

            Else
                MessageBox.Show("No se pudo Modificar")
            End If
            conx.Close()



        End If
    End Sub


    Private Sub Limpliar()
        TxtNombre.Text = ""
        TxtApellido.Text = ""
        TxtDireccion.Text = ""
        TxtDpi.Text = ""
        MonthCalendar1.SetDate(MonthCalendar1.TodayDate)
    End Sub
End Class