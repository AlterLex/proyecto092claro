﻿Imports System.Data.OleDb

Public Class ModificarCategoria
    Private Sub BRegresar_Click(sender As Object, e As EventArgs) Handles BRegresar.Click
        Dim regreso = New Categoria
        regreso.Show()
        Me.Hide()
    End Sub

    Private Sub ModificarCategoria_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Categoria]"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Categorias")
        ComboCategoria.DataSource = Data.Tables(0)
        ComboCategoria.DisplayMember = Data.Tables(0).Columns(0).Caption.ToString()
        ComboCategoria.ValueMember = Data.Tables(0).Columns(1).Caption.ToString()
        Dim ComboSelecionado As String = ComboCategoria.SelectedValue.ToString
        TxtCategoria.Text = ComboSelecionado
        conx.Close()
    End Sub

    Private Sub ComboCategoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboCategoria.SelectedIndexChanged
        Dim ComboSelecionado As String = ComboCategoria.SelectedValue.ToString
        TxtCategoria.Text = ComboSelecionado
    End Sub

    Private Sub BModificar_Click(sender As Object, e As EventArgs) Handles BModificar.Click
        Dim Desicion As Integer = MessageBox.Show("Desea modificar categoria", "Modificar Categoria", MessageBoxButtons.YesNo)

        If Desicion = DialogResult.Yes Then
            Dim conx As OleDbConnection = Conexion.ConexionBD
            Dim Cadena As String = "Update [Categoria] set [Descripcion]=@texto where [CodigoCategoria]=@numero "
            Dim Comando As New OleDbCommand(Cadena, conx)
            Comando.Parameters.AddWithValue("@texto", TxtCategoria.Text)
            Comando.Parameters.AddWithValue("@numero", ComboCategoria.Text)
            Dim i As Int32 = Comando.ExecuteNonQuery()
            If i > 0 Then
                MessageBox.Show("Modificado con exito")
            Else
                MessageBox.Show("No se pudo Modificar")
            End If
            Dim Cadena1 As String = "Select * from [Categoria]"
            Dim Adaptador = New OleDbDataAdapter(Cadena1, conx)
            Dim Data As New DataSet
            Adaptador.Fill(Data, "Categorias")
            ComboCategoria.DataSource = Data.Tables(0)
            ComboCategoria.DisplayMember = Data.Tables(0).Columns(0).Caption.ToString()
            ComboCategoria.ValueMember = Data.Tables(0).Columns(1).Caption.ToString()
            Dim ComboSelecionado As String = ComboCategoria.SelectedValue.ToString
            TxtCategoria.Text = ComboSelecionado
            conx.Close()
        End If

    End Sub
End Class