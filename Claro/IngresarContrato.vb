﻿Imports System.Data.OleDb

Public Class IngresarContrato
    Private Sub Regresar_Click(sender As Object, e As EventArgs) Handles Regresar.Click
        Dim regreso = New Contrato
        regreso.Show()
        Me.Hide()
    End Sub

    Private Sub IngresarContrato_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Servicio]"
        Dim Cadena1 As String = "Select * from [Linea]"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Adaptador1 = New OleDbDataAdapter(Cadena1, conx)
        Dim Data As New DataSet
        Dim Data1 As New DataSet
        Adaptador.Fill(Data, "Servicio")
        ComboServicio.DataSource = Data.Tables(0)
        ComboServicio.DisplayMember = Data.Tables(0).Columns(1).Caption.ToString()
        ComboServicio.ValueMember = Data.Tables(0).Columns(0).Caption.ToString()
        Adaptador1.Fill(Data1, "Linea")
        ComboLinea.DataSource = Data1.Tables(0)
        ComboLinea.DisplayMember = Data1.Tables(0).Columns(2).Caption.ToString()
        ComboLinea.ValueMember = Data1.Tables(0).Columns(0).Caption.ToString()
        conx.Close()
    End Sub

    Private Sub Ingresar_Click(sender As Object, e As EventArgs) Handles Ingresar.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "insert into [Contrato]([CodigoLinea],[CodigoServicio],[Fecha],[Vigencia],[Costo]) values (@lin,@ser,@fe,@vi,@cos) "
        Dim Comando As New OleDbCommand(Cadena, conx)
        Comando.Parameters.AddWithValue("@lin", ComboLinea.SelectedValue.ToString())
        Comando.Parameters.AddWithValue("@ser", ComboServicio.SelectedValue.ToString())
        Comando.Parameters.AddWithValue("@fe", MonthCalendar1.SelectionRange.Start)
        Comando.Parameters.AddWithValue("@vi", TxtVigencia.Text)
        Comando.Parameters.AddWithValue("@cos", TxtCosto.Text)

        Dim i As Int32 = Comando.ExecuteNonQuery()
        If i > 0 Then
            MessageBox.Show("Insertado con exito")
            Limpliar()
        Else
            MessageBox.Show("No se pudo Insertar")
        End If
        conx.Close()
    End Sub

    Private Sub Limpliar()
        TxtVigencia.Text = ""
        TxtCosto.Text = ""
        MonthCalendar1.SetDate(MonthCalendar1.TodayDate)
    End Sub
End Class