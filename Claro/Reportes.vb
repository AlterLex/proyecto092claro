﻿Imports System.Data.OleDb

Public Class Reportes
    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim menu = New MenuPrincipal
        menu.Show()
        Me.Hide()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "SELECT Count(Contrato.CodigoServicio) AS NumeroDeContratos, Servicio.Descripcion AS Servicio
        FROM Servicio INNER JOIN Contrato ON Servicio.CodigoServicio = Contrato.CodigoServicio
        GROUP BY Servicio.Descripcion, Contrato.CodigoServicio;"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Consulta1")
        DataGridView1.DataSource = ""
        DataGridView1.DataSource = Data.Tables(0).DefaultView
        conx.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "SELECT Cliente.Nombre, Count(Contrato.CodigoServicio) AS CuentaDeCodigoServicio
        FROM Servicio INNER JOIN ((Cliente INNER JOIN Linea ON Cliente.CodigoCliente = Linea.CodigoCliente) INNER JOIN Contrato ON Linea.CodigoLinea = Contrato.CodigoLinea) ON Servicio.CodigoServicio = Contrato.CodigoServicio
        GROUP BY Cliente.Nombre;"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Consulta1")
        DataGridView1.DataSource = ""
        DataGridView1.DataSource = Data.Tables(0).DefaultView
        conx.Close()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "SELECT Contrato.CodigoContrato, Contrato.Vigencia
        FROM Contrato ORDER BY Contrato.Vigencia;"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Consulta1")
        DataGridView1.DataSource = ""
        DataGridView1.DataSource = Data.Tables(0).DefaultView
        conx.Close()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select Case Categoria.CodigoCategoria, Linea.Telefono
        From Categoria INNER Join Linea On Categoria.CodigoCategoria = Linea.CodigoCategoria
        Group By Categoria.CodigoCategoria, Linea.Telefono;"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Consulta1")
        DataGridView1.DataSource = ""
        DataGridView1.DataSource = Data.Tables(0).DefaultView
        conx.Close()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        Dim i As Integer
        Try
            i = InputBox("Ingrese un numero")
            Dim conx As OleDbConnection = Conexion.ConexionBD
            Dim Cadena As String = "SELECT Linea.CodigoLinea, Linea.Telefono
            FROM Cliente INNER JOIN Linea ON Cliente.CodigoCliente = Linea.CodigoCliente
            WHERE (((Cliente.CodigoCliente)=@num));"
            Dim comando As OleDbCommand = New OleDbCommand(Cadena, conx)
            comando.Parameters.AddWithValue("@num", i)
            Dim Adaptador = New OleDbDataAdapter
            Adaptador.SelectCommand = comando
            Dim Data As New DataSet
            Adaptador.Fill(Data, "Consulta1")
            DataGridView1.DataSource = ""
            DataGridView1.DataSource = Data.Tables(0).DefaultView
            conx.Close()
        Catch

            MsgBox("No ingreso un numero")
        End Try

    End Sub
End Class