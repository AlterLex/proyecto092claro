﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class IngresarCategoria
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Ingresar = New System.Windows.Forms.Button()
        Me.Regresar = New System.Windows.Forms.Button()
        Me.LCateroria = New System.Windows.Forms.Label()
        Me.LTitulo = New System.Windows.Forms.Label()
        Me.TextoCategoria = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Ingresar
        '
        Me.Ingresar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ingresar.Location = New System.Drawing.Point(16, 151)
        Me.Ingresar.Name = "Ingresar"
        Me.Ingresar.Size = New System.Drawing.Size(107, 42)
        Me.Ingresar.TabIndex = 0
        Me.Ingresar.Text = "Ingresar"
        Me.Ingresar.UseVisualStyleBackColor = True
        '
        'Regresar
        '
        Me.Regresar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Regresar.Location = New System.Drawing.Point(145, 151)
        Me.Regresar.Name = "Regresar"
        Me.Regresar.Size = New System.Drawing.Size(113, 42)
        Me.Regresar.TabIndex = 1
        Me.Regresar.Text = "Regresar"
        Me.Regresar.UseVisualStyleBackColor = True
        '
        'LCateroria
        '
        Me.LCateroria.AutoSize = True
        Me.LCateroria.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCateroria.Location = New System.Drawing.Point(12, 100)
        Me.LCateroria.Name = "LCateroria"
        Me.LCateroria.Size = New System.Drawing.Size(78, 20)
        Me.LCateroria.TabIndex = 2
        Me.LCateroria.Text = "Categoria"
        '
        'LTitulo
        '
        Me.LTitulo.AutoSize = True
        Me.LTitulo.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LTitulo.Location = New System.Drawing.Point(40, 27)
        Me.LTitulo.Name = "LTitulo"
        Me.LTitulo.Size = New System.Drawing.Size(189, 25)
        Me.LTitulo.TabIndex = 3
        Me.LTitulo.Text = "Ingresar Categoria"
        '
        'TextoCategoria
        '
        Me.TextoCategoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextoCategoria.Location = New System.Drawing.Point(107, 94)
        Me.TextoCategoria.Name = "TextoCategoria"
        Me.TextoCategoria.Size = New System.Drawing.Size(165, 26)
        Me.TextoCategoria.TabIndex = 4
        '
        'IngresarCategoria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 222)
        Me.Controls.Add(Me.TextoCategoria)
        Me.Controls.Add(Me.LTitulo)
        Me.Controls.Add(Me.LCateroria)
        Me.Controls.Add(Me.Regresar)
        Me.Controls.Add(Me.Ingresar)
        Me.Name = "IngresarCategoria"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "IngresarCategoria"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Ingresar As Button
    Friend WithEvents Regresar As Button
    Friend WithEvents LCateroria As Label
    Friend WithEvents LTitulo As Label
    Friend WithEvents TextoCategoria As TextBox
End Class
