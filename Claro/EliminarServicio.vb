﻿Imports System.Data.OleDb

Public Class EliminarServicio
    Private Sub BRegresar_Click(sender As Object, e As EventArgs) Handles BRegresar.Click
        Dim regreso = New Servicio
        regreso.Show()
        Me.Hide()
    End Sub

    Private Sub EliminarServicio_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Servicio]"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Servicio")
        ComboServicio.DataSource = Data.Tables(0)
        ComboServicio.DisplayMember = Data.Tables(0).Columns(0).Caption.ToString()
        ComboServicio.ValueMember = Data.Tables(0).Columns(1).Caption.ToString()
        Dim ComboSelecionado As String = ComboServicio.SelectedValue.ToString
        TxtServicio.Text = ComboSelecionado
        conx.Close()
    End Sub

    Private Sub ComboCategoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboServicio.SelectedIndexChanged
        Dim ComboSelecionado As String = ComboServicio.SelectedValue.ToString
        TxtServicio.Text = ComboSelecionado
    End Sub

    Private Sub BEliminar_Click(sender As Object, e As EventArgs) Handles BEliminar.Click
        Dim Desicion As Integer = MessageBox.Show("Desea eliminar Servicio", "Eliminar Servicio", MessageBoxButtons.YesNo)

        If Desicion = DialogResult.Yes Then
            Dim conx As OleDbConnection = Conexion.ConexionBD
            Dim Cadena As String = "delete from [Servicio] where [CodigoServicio]=@numero "
            Dim Comando As New OleDbCommand(Cadena, conx)
            Comando.Parameters.AddWithValue("@numero", ComboServicio.Text)
            Dim i As Int32 = Comando.ExecuteNonQuery()
            If i > 0 Then
                MessageBox.Show("Eliminada con exito")
            Else
                MessageBox.Show("No se pudo Eliminar")
            End If
            Dim Cadena1 As String = "Select * from [Servicio]"
            Dim Adaptador = New OleDbDataAdapter(Cadena1, conx)
            Dim Data As New DataSet
            Adaptador.Fill(Data, "Servicio")
            ComboServicio.DataSource = Data.Tables(0)
            ComboServicio.DisplayMember = Data.Tables(0).Columns(0).Caption.ToString()
            ComboServicio.ValueMember = Data.Tables(0).Columns(1).Caption.ToString()
            Dim ComboSelecionado As String = ComboServicio.SelectedValue.ToString
            TxtServicio.Text = ComboSelecionado
            conx.Close()
        End If
    End Sub
End Class