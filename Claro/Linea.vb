﻿Public Class Linea
    Private Sub BRegresar_Click(sender As Object, e As EventArgs) Handles BRegresar.Click
        Dim RegresarMenuPrincipal = New MenuPrincipal
        RegresarMenuPrincipal.Show()
        Me.Hide()

    End Sub

    Private Sub IrInsertar_Click(sender As Object, e As EventArgs) Handles IrInsertar.Click
        Dim IrIngresarLinea = New IngresarLinea
        IrIngresarLinea.Show()
        Me.Hide()
    End Sub

    Private Sub IrModificar_Click(sender As Object, e As EventArgs) Handles IrModificar.Click
        Dim IrModicarLinea = New ModificarLinea
        IrModicarLinea.Show()
        Me.Hide()

    End Sub

    Private Sub IrEliminar_Click(sender As Object, e As EventArgs) Handles IrEliminar.Click
        Dim IrEliminarLinea = New EliminarLinea
        IrEliminarLinea.Show()
        Me.Hide()

    End Sub
End Class