﻿Public Class Categoria
    Private Sub BRegresar_Click(sender As Object, e As EventArgs) Handles BRegresar.Click
        Dim RegresarMenuPrincipal = New MenuPrincipal
        RegresarMenuPrincipal.Show()
        Me.Hide()

    End Sub

    Private Sub IrInsertar_Click(sender As Object, e As EventArgs) Handles IrInsertar.Click
        Dim IrInsertarCategoria = New IngresarCategoria
        IrInsertarCategoria.Show()
        Me.Hide()
    End Sub

    Private Sub IrModificar_Click(sender As Object, e As EventArgs) Handles IrModificar.Click
        Dim IrModicarCategoria = New ModificarCategoria
        IrModicarCategoria.Show()
        Me.Hide()

    End Sub

    Private Sub IrEliminar_Click(sender As Object, e As EventArgs) Handles IrEliminar.Click
        Dim IrEliminarCategoria = New EliminarCategoria
        IrEliminarCategoria.Show()
        Me.Hide()

    End Sub


End Class