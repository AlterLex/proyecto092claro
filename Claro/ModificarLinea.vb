﻿Imports System.Data.OleDb

Public Class ModificarLinea
    Private Sub Regresar_Click(sender As Object, e As EventArgs) Handles Regresar.Click
        Dim regreso = New Linea
        regreso.Show()
        Me.Hide()
    End Sub

    Private Sub ModificarLinea_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Categoria]"
        Dim Cadena1 As String = "Select * from [Cliente]"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Adaptador1 = New OleDbDataAdapter(Cadena1, conx)
        Dim Data As New DataSet
        Dim Data1 As New DataSet
        Adaptador.Fill(Data, "Categorias")
        ComboCategoria.DataSource = Data.Tables(0)
        ComboCategoria.DisplayMember = Data.Tables(0).Columns(1).Caption.ToString()
        ComboCategoria.ValueMember = Data.Tables(0).Columns(0).Caption.ToString()
        Adaptador1.Fill(Data1, "Clientes")
        ComboCliente.DataSource = Data1.Tables(0)
        ComboCliente.DisplayMember = Data1.Tables(0).Columns(5).Caption.ToString()
        ComboCliente.ValueMember = Data1.Tables(0).Columns(0).Caption.ToString()
        conx.Close()
    End Sub

    Private Sub BBuscar_Click(sender As Object, e As EventArgs) Handles BBuscar.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Linea] where [CodigoLinea]=@lin"
        Dim Comando As OleDbCommand = New OleDbCommand(Cadena, conx)
        Comando.Parameters.AddWithValue("@lin", TxtLinea.Text)
        Dim Adaptador = New OleDbDataAdapter()
        Adaptador.SelectCommand = Comando
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Linea")
        If ((Data.Tables.Count > 0) AndAlso (Data.Tables(0).Rows.Count > 0)) Then
            TxtLinea.Enabled = False
            TxtTelefono.Enabled = True
            ComboCategoria.Enabled = True
            ComboCliente.Enabled = True
            Modificar.Enabled = True

            ComboCategoria.SelectedValue = Data.Tables(0).Rows(0).Item(1).ToString()
            TxtTelefono.Text = Data.Tables(0).Rows(0).Item(2).ToString()
            ComboCliente.SelectedValue = Data.Tables(0).Rows(0).Item(3).ToString()

        Else
            MessageBox.Show("No existe Linea con ese codigo")
        End If
        conx.Close()
    End Sub

    Private Sub Modificar_Click(sender As Object, e As EventArgs) Handles Modificar.Click
        Dim Desicion As Integer = MessageBox.Show("Desea modificar Linea", "Modificar Linea", MessageBoxButtons.YesNo)

        If Desicion = DialogResult.Yes Then

            Dim conx As OleDbConnection = Conexion.ConexionBD
            Dim Cadena As String = " update [Linea] set [CodigoCategoria]=@cate,[Telefono]=@tel,[CodigoCliente]=@clien where [CodigoLinea]=@num "
            Dim Comando As New OleDbCommand(Cadena, conx)
            Comando.Parameters.AddWithValue("@cate", ComboCategoria.SelectedValue.ToString())
            Comando.Parameters.AddWithValue("@tel", TxtTelefono.Text)
            Comando.Parameters.AddWithValue("@clien", ComboCliente.SelectedValue.ToString())
            Comando.Parameters.AddWithValue("@num", TxtLinea.Text)
            Dim i As Int32 = Comando.ExecuteNonQuery()
            If i > 0 Then
                MessageBox.Show("Modificado con exito")
                BBuscar.Enabled = True
                Modificar.Enabled = False
                TxtTelefono.Enabled = False
                TxtTelefono.Text = ""
                ComboCategoria.Enabled = False
                ComboCliente.Enabled = False
                TxtLinea.Enabled = True

            Else
                MessageBox.Show("No se pudo Modificar")
            End If
            conx.Close()
        End If

    End Sub
End Class