﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MenuPrincipal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.IrCiente = New System.Windows.Forms.Button()
        Me.LabelMenuPrincipal = New System.Windows.Forms.Label()
        Me.IrCategoria = New System.Windows.Forms.Button()
        Me.VerContratante = New System.Windows.Forms.Button()
        Me.VerLinea = New System.Windows.Forms.Button()
        Me.VerReportes = New System.Windows.Forms.Button()
        Me.VerServicio = New System.Windows.Forms.Button()
        Me.ExitPrograma = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'IrCiente
        '
        Me.IrCiente.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IrCiente.Location = New System.Drawing.Point(182, 97)
        Me.IrCiente.Name = "IrCiente"
        Me.IrCiente.Size = New System.Drawing.Size(118, 36)
        Me.IrCiente.TabIndex = 0
        Me.IrCiente.Text = "Cliente"
        Me.IrCiente.UseVisualStyleBackColor = True
        '
        'LabelMenuPrincipal
        '
        Me.LabelMenuPrincipal.AutoSize = True
        Me.LabelMenuPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelMenuPrincipal.Location = New System.Drawing.Point(153, 42)
        Me.LabelMenuPrincipal.Name = "LabelMenuPrincipal"
        Me.LabelMenuPrincipal.Size = New System.Drawing.Size(173, 29)
        Me.LabelMenuPrincipal.TabIndex = 1
        Me.LabelMenuPrincipal.Text = "Menu Principal"
        '
        'IrCategoria
        '
        Me.IrCategoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IrCategoria.Location = New System.Drawing.Point(21, 97)
        Me.IrCategoria.Name = "IrCategoria"
        Me.IrCategoria.Size = New System.Drawing.Size(118, 36)
        Me.IrCategoria.TabIndex = 2
        Me.IrCategoria.Text = "Categoria"
        Me.IrCategoria.UseVisualStyleBackColor = True
        '
        'VerContratante
        '
        Me.VerContratante.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VerContratante.Location = New System.Drawing.Point(335, 97)
        Me.VerContratante.Name = "VerContratante"
        Me.VerContratante.Size = New System.Drawing.Size(118, 36)
        Me.VerContratante.TabIndex = 3
        Me.VerContratante.Text = "Contratante"
        Me.VerContratante.UseVisualStyleBackColor = True
        '
        'VerLinea
        '
        Me.VerLinea.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VerLinea.Location = New System.Drawing.Point(21, 139)
        Me.VerLinea.Name = "VerLinea"
        Me.VerLinea.Size = New System.Drawing.Size(118, 36)
        Me.VerLinea.TabIndex = 4
        Me.VerLinea.Text = "Linea"
        Me.VerLinea.UseVisualStyleBackColor = True
        '
        'VerReportes
        '
        Me.VerReportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VerReportes.Location = New System.Drawing.Point(182, 139)
        Me.VerReportes.Name = "VerReportes"
        Me.VerReportes.Size = New System.Drawing.Size(118, 36)
        Me.VerReportes.TabIndex = 5
        Me.VerReportes.Text = "Reportes"
        Me.VerReportes.UseVisualStyleBackColor = True
        '
        'VerServicio
        '
        Me.VerServicio.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VerServicio.Location = New System.Drawing.Point(335, 139)
        Me.VerServicio.Name = "VerServicio"
        Me.VerServicio.Size = New System.Drawing.Size(118, 36)
        Me.VerServicio.TabIndex = 6
        Me.VerServicio.Text = "Servicio"
        Me.VerServicio.UseVisualStyleBackColor = True
        '
        'ExitPrograma
        '
        Me.ExitPrograma.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExitPrograma.Location = New System.Drawing.Point(182, 181)
        Me.ExitPrograma.Name = "ExitPrograma"
        Me.ExitPrograma.Size = New System.Drawing.Size(118, 36)
        Me.ExitPrograma.TabIndex = 7
        Me.ExitPrograma.Text = "Salir"
        Me.ExitPrograma.UseVisualStyleBackColor = True
        '
        'MenuPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(482, 236)
        Me.Controls.Add(Me.ExitPrograma)
        Me.Controls.Add(Me.VerServicio)
        Me.Controls.Add(Me.VerReportes)
        Me.Controls.Add(Me.VerLinea)
        Me.Controls.Add(Me.VerContratante)
        Me.Controls.Add(Me.IrCategoria)
        Me.Controls.Add(Me.LabelMenuPrincipal)
        Me.Controls.Add(Me.IrCiente)
        Me.Name = "MenuPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MenuPrincipal"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents IrCiente As Button
    Friend WithEvents LabelMenuPrincipal As Label
    Friend WithEvents IrCategoria As Button
    Friend WithEvents VerContratante As Button
    Friend WithEvents VerLinea As Button
    Friend WithEvents VerReportes As Button
    Friend WithEvents VerServicio As Button
    Friend WithEvents ExitPrograma As Button
End Class
