﻿Public Class Servicio
    Private Sub BRegresar_Click(sender As Object, e As EventArgs) Handles BRegresar.Click
        Dim RegresarMenuPrincipal = New MenuPrincipal
        RegresarMenuPrincipal.Show()
        Me.Hide()
    End Sub

    Private Sub IrInsertar_Click(sender As Object, e As EventArgs) Handles IrInsertar.Click
        Dim IrInsertarServicio = New IngresarServicio
        IrInsertarServicio.Show()
        Me.Hide()
    End Sub

    Private Sub IrModificar_Click(sender As Object, e As EventArgs) Handles IrModificar.Click
        Dim IrModificarServicio = New ModificarServicio
        IrModificarServicio.Show()
        Me.Hide()
    End Sub

    Private Sub IrEliminar_Click(sender As Object, e As EventArgs) Handles IrEliminar.Click
        Dim IrEliminarServicio = New EliminarServicio
        IrEliminarServicio.Show()
        Me.Hide()
    End Sub
End Class