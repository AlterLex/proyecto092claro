﻿Imports System.Data.OleDb

Public Class IngresarServicio
    Private Sub Regresar_Click(sender As Object, e As EventArgs) Handles Regresar.Click
        Dim Regreso = New Servicio
        Regreso.Show()
        Me.Hide()
    End Sub

    Private Sub Ingresar_Click(sender As Object, e As EventArgs) Handles Ingresar.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "insert into [Servicio]([Descripcion]) values (@ser) "
        Dim Comando As New OleDbCommand(Cadena, conx)
        Comando.Parameters.AddWithValue("@ser", TxtServicio.Text)
        Dim i As Int32 = Comando.ExecuteNonQuery()
        If i > 0 Then
            MessageBox.Show("Insertado con exito")
        Else
            MessageBox.Show("No se pudo Insertar")
        End If
        conx.Close()
        TxtServicio.Text = ""
    End Sub
End Class