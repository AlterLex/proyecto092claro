﻿Imports System.Data.OleDb

Public Class EliminarCliente
    Private Sub BBuscar_Click(sender As Object, e As EventArgs) Handles BBuscar.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Cliente] where [CodigoCliente]=@cod"
        Dim comando = New OleDbCommand(Cadena, conx)
        comando.Parameters.AddWithValue("@cod", TxtCodigo.Text)
        Dim Adaptador = New OleDbDataAdapter()
        Adaptador.SelectCommand = comando
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Clientes")
        If ((Data.Tables.Count > 0) AndAlso (Data.Tables(0).Rows.Count > 0)) Then

            TxtNombre.Text = Data.Tables(0).Rows(0).Item(1).ToString()
            TxtApellido.Text = Data.Tables(0).Rows(0).Item(2).ToString()
            Dim fecha As Date = Date.Parse(Data.Tables(0).Rows(0).Item(3).ToString())
            MonthCalendar1.SetDate(fecha)
            TxtDireccion.Text = Data.Tables(0).Rows(0).Item(4).ToString()
            TxtDpi.Text = Data.Tables(0).Rows(0).Item(5).ToString()

        Else
            MessageBox.Show("No existe Cliente con ese codigo")
        End If
        conx.Close()
    End Sub

    Private Sub Regresar_Click(sender As Object, e As EventArgs) Handles Regresar.Click
        Dim regreso = New Cliente
        regreso.Show()
        Me.Hide()

    End Sub

    Private Sub BEliminar_Click(sender As Object, e As EventArgs) Handles BEliminar.Click
        Dim Desicion As Integer = MessageBox.Show("Desea Eliminar cliente", "Eliminar Cliente", MessageBoxButtons.YesNo)

        If Desicion = DialogResult.Yes Then
            Dim conx As OleDbConnection = Conexion.ConexionBD
            Dim Cadena As String = "delete from [Cliente]  where [CodigoCliente]=@numero "
            Dim Comando As New OleDbCommand(Cadena, conx)
            Comando.Parameters.AddWithValue("@numero", TxtCodigo.Text)
            Dim i As Integer = Comando.ExecuteNonQuery()


            If i > 0 Then
                MessageBox.Show("Eliminado con exito")
                Limpliar()

            Else
                MessageBox.Show("No se pudo Eliminar")
            End If
            conx.Close()
        End If

    End Sub

    Private Sub Limpliar()
        TxtNombre.Text = ""
        TxtApellido.Text = ""
        TxtDireccion.Text = ""
        TxtDpi.Text = ""
        MonthCalendar1.SetDate(MonthCalendar1.TodayDate)
    End Sub
End Class