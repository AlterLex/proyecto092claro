﻿Public Class Contrato
    Private Sub BRegresar_Click(sender As Object, e As EventArgs) Handles BRegresar.Click
        Dim RegresarMenuPrincipal = New MenuPrincipal
        RegresarMenuPrincipal.Show()
        Me.Hide()

    End Sub

    Private Sub IrInsertar_Click(sender As Object, e As EventArgs) Handles IrInsertar.Click
        Dim IrIngresarContrato = New IngresarContrato
        IrIngresarContrato.Show()
        Me.Hide()
    End Sub

    Private Sub IrModificar_Click(sender As Object, e As EventArgs) Handles IrModificar.Click
        Dim IrModicarContrato = New ModificarContrato
        IrModicarContrato.Show()
        Me.Hide()

    End Sub

    Private Sub IrEliminar_Click(sender As Object, e As EventArgs) Handles IrEliminar.Click
        Dim IrEliminarContrato = New EliminarContrato
        IrEliminarContrato.Show()
        Me.Hide()

    End Sub
End Class