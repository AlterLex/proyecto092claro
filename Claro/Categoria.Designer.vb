﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Categoria
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.IrInsertar = New System.Windows.Forms.Button()
        Me.IrModificar = New System.Windows.Forms.Button()
        Me.IrEliminar = New System.Windows.Forms.Button()
        Me.BRegresar = New System.Windows.Forms.Button()
        Me.LCategoria = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'IrInsertar
        '
        Me.IrInsertar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IrInsertar.Location = New System.Drawing.Point(41, 82)
        Me.IrInsertar.Name = "IrInsertar"
        Me.IrInsertar.Size = New System.Drawing.Size(122, 31)
        Me.IrInsertar.TabIndex = 0
        Me.IrInsertar.Text = "Insertar"
        Me.IrInsertar.UseVisualStyleBackColor = True
        '
        'IrModificar
        '
        Me.IrModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IrModificar.Location = New System.Drawing.Point(41, 119)
        Me.IrModificar.Name = "IrModificar"
        Me.IrModificar.Size = New System.Drawing.Size(122, 31)
        Me.IrModificar.TabIndex = 1
        Me.IrModificar.Text = "Modificar"
        Me.IrModificar.UseVisualStyleBackColor = True
        '
        'IrEliminar
        '
        Me.IrEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IrEliminar.Location = New System.Drawing.Point(41, 156)
        Me.IrEliminar.Name = "IrEliminar"
        Me.IrEliminar.Size = New System.Drawing.Size(122, 31)
        Me.IrEliminar.TabIndex = 2
        Me.IrEliminar.Text = "Eliminar"
        Me.IrEliminar.UseVisualStyleBackColor = True
        '
        'BRegresar
        '
        Me.BRegresar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BRegresar.Location = New System.Drawing.Point(43, 212)
        Me.BRegresar.Name = "BRegresar"
        Me.BRegresar.Size = New System.Drawing.Size(120, 31)
        Me.BRegresar.TabIndex = 3
        Me.BRegresar.Text = "Regresar"
        Me.BRegresar.UseVisualStyleBackColor = True
        '
        'LCategoria
        '
        Me.LCategoria.AutoSize = True
        Me.LCategoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCategoria.Location = New System.Drawing.Point(58, 29)
        Me.LCategoria.Name = "LCategoria"
        Me.LCategoria.Size = New System.Drawing.Size(105, 25)
        Me.LCategoria.TabIndex = 4
        Me.LCategoria.Text = "Categoria"
        '
        'Categoria
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(211, 261)
        Me.Controls.Add(Me.LCategoria)
        Me.Controls.Add(Me.BRegresar)
        Me.Controls.Add(Me.IrEliminar)
        Me.Controls.Add(Me.IrModificar)
        Me.Controls.Add(Me.IrInsertar)
        Me.Name = "Categoria"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Categoria"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents IrInsertar As Button
    Friend WithEvents IrModificar As Button
    Friend WithEvents IrEliminar As Button
    Friend WithEvents BRegresar As Button
    Friend WithEvents LCategoria As Label
End Class
