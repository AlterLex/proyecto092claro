﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Servicio
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LCliente = New System.Windows.Forms.Label()
        Me.BRegresar = New System.Windows.Forms.Button()
        Me.IrEliminar = New System.Windows.Forms.Button()
        Me.IrModificar = New System.Windows.Forms.Button()
        Me.IrInsertar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LCliente
        '
        Me.LCliente.AutoSize = True
        Me.LCliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LCliente.Location = New System.Drawing.Point(44, 24)
        Me.LCliente.Name = "LCliente"
        Me.LCliente.Size = New System.Drawing.Size(89, 25)
        Me.LCliente.TabIndex = 14
        Me.LCliente.Text = "Servicio"
        '
        'BRegresar
        '
        Me.BRegresar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BRegresar.Location = New System.Drawing.Point(29, 207)
        Me.BRegresar.Name = "BRegresar"
        Me.BRegresar.Size = New System.Drawing.Size(120, 31)
        Me.BRegresar.TabIndex = 13
        Me.BRegresar.Text = "Regresar"
        Me.BRegresar.UseVisualStyleBackColor = True
        '
        'IrEliminar
        '
        Me.IrEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IrEliminar.Location = New System.Drawing.Point(27, 151)
        Me.IrEliminar.Name = "IrEliminar"
        Me.IrEliminar.Size = New System.Drawing.Size(122, 31)
        Me.IrEliminar.TabIndex = 12
        Me.IrEliminar.Text = "Eliminar"
        Me.IrEliminar.UseVisualStyleBackColor = True
        '
        'IrModificar
        '
        Me.IrModificar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IrModificar.Location = New System.Drawing.Point(27, 114)
        Me.IrModificar.Name = "IrModificar"
        Me.IrModificar.Size = New System.Drawing.Size(122, 31)
        Me.IrModificar.TabIndex = 11
        Me.IrModificar.Text = "Modificar"
        Me.IrModificar.UseVisualStyleBackColor = True
        '
        'IrInsertar
        '
        Me.IrInsertar.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.IrInsertar.Location = New System.Drawing.Point(27, 77)
        Me.IrInsertar.Name = "IrInsertar"
        Me.IrInsertar.Size = New System.Drawing.Size(122, 31)
        Me.IrInsertar.TabIndex = 10
        Me.IrInsertar.Text = "Insertar"
        Me.IrInsertar.UseVisualStyleBackColor = True
        '
        'Servicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(177, 262)
        Me.Controls.Add(Me.LCliente)
        Me.Controls.Add(Me.BRegresar)
        Me.Controls.Add(Me.IrEliminar)
        Me.Controls.Add(Me.IrModificar)
        Me.Controls.Add(Me.IrInsertar)
        Me.Name = "Servicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Servicio"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LCliente As Label
    Friend WithEvents BRegresar As Button
    Friend WithEvents IrEliminar As Button
    Friend WithEvents IrModificar As Button
    Friend WithEvents IrInsertar As Button
End Class
