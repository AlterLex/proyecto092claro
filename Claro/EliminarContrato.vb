﻿Imports System.Data.OleDb

Public Class EliminarContrato
    Private Sub EliminarContrato_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Servicio]"
        Dim Cadena1 As String = "Select * from [Linea]"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Adaptador1 = New OleDbDataAdapter(Cadena1, conx)
        Dim Data As New DataSet
        Dim Data1 As New DataSet
        Adaptador.Fill(Data, "Servicio")
        ComboServicio.DataSource = Data.Tables(0)
        ComboServicio.DisplayMember = Data.Tables(0).Columns(1).Caption.ToString()
        ComboServicio.ValueMember = Data.Tables(0).Columns(0).Caption.ToString()
        Adaptador1.Fill(Data1, "Linea")
        ComboLinea.DataSource = Data1.Tables(0)
        ComboLinea.DisplayMember = Data1.Tables(0).Columns(2).Caption.ToString()
        ComboLinea.ValueMember = Data1.Tables(0).Columns(0).Caption.ToString()
        conx.Close()
    End Sub


    Private Sub BBuscar_Click(sender As Object, e As EventArgs) Handles BBuscar.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Contrato] where [CodigoContrato]=@cod"
        Dim comando = New OleDbCommand(Cadena, conx)
        comando.Parameters.AddWithValue("@cod", TxtContrato.Text)
        Dim Adaptador = New OleDbDataAdapter()
        Adaptador.SelectCommand = comando
        Dim Data As New DataSet
        Adaptador.Fill(Data, "Contrato")
        If ((Data.Tables.Count > 0) AndAlso (Data.Tables(0).Rows.Count > 0)) Then
            Modificar.Enabled = True

            ComboLinea.SelectedValue = Data.Tables(0).Rows(0).Item(1).ToString()
            ComboServicio.SelectedValue = Data.Tables(0).Rows(0).Item(2).ToString()
            Dim fecha As Date = Date.Parse(Data.Tables(0).Rows(0).Item(3).ToString())
            MonthCalendar1.SetDate(fecha)
            TxtVigencia.Text = Data.Tables(0).Rows(0).Item(4).ToString()
            TxtCosto.Text = Data.Tables(0).Rows(0).Item(5).ToString()

        Else
            MessageBox.Show("No existe Contrato con ese codigo")
        End If
        conx.Close()
    End Sub

    Private Sub Regresar_Click(sender As Object, e As EventArgs) Handles Regresar.Click
        Dim regreso = New Contrato
        regreso.Show()
        Me.Hide()
    End Sub

    Private Sub Modificar_Click(sender As Object, e As EventArgs) Handles Modificar.Click
        Dim Desicion As Integer = MessageBox.Show("Desea eliminar contrato", "Eliminar Contrato", MessageBoxButtons.YesNo)

        If Desicion = DialogResult.Yes Then
            Dim conx As OleDbConnection = Conexion.ConexionBD
            Dim Cadena As String = "delete From [Contrato] where [CodigoContrato]=@numero "
            Dim Comando As New OleDbCommand(Cadena, conx)
            Comando.Parameters.AddWithValue("@numero", TxtContrato.Text)



            Dim i As Integer = Comando.ExecuteNonQuery()


            If i > 0 Then
                MessageBox.Show("Eliminar con exito")
                ComboLinea.Enabled = False
                ComboServicio.Enabled = False
                TxtContrato.Enabled = True
                TxtCosto.Enabled = False
                TxtVigencia.Enabled = False
                BBuscar.Enabled = True
                Modificar.Enabled = False
                MonthCalendar1.Enabled = False
                TxtVigencia.Text = ""
                TxtCosto.Text = ""

            Else
                MessageBox.Show("No se pudo Eliminar")
            End If
            conx.Close()



        End If
    End Sub
End Class