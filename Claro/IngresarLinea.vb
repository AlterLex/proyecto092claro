﻿Imports System.Data.OleDb

Public Class IngresarLinea
    Private Sub Regresar_Click(sender As Object, e As EventArgs) Handles Regresar.Click
        Dim Regreso = New Linea
        Regreso.Show()
        Me.Hide()
    End Sub

    Private Sub IngresarLinea_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "Select * from [Categoria]"
        Dim Cadena1 As String = "Select * from [Cliente]"
        Dim Adaptador = New OleDbDataAdapter(Cadena, conx)
        Dim Adaptador1 = New OleDbDataAdapter(Cadena1, conx)
        Dim Data As New DataSet
        Dim Data1 As New DataSet
        Adaptador.Fill(Data, "Categorias")
        ComboCategoria.DataSource = Data.Tables(0)
        ComboCategoria.DisplayMember = Data.Tables(0).Columns(1).Caption.ToString()
        ComboCategoria.ValueMember = Data.Tables(0).Columns(0).Caption.ToString()
        Adaptador1.Fill(Data1, "Clientes")
        ComboCliente.DataSource = Data1.Tables(0)
        ComboCliente.DisplayMember = Data1.Tables(0).Columns(5).Caption.ToString()
        ComboCliente.ValueMember = Data1.Tables(0).Columns(0).Caption.ToString()
        conx.Close()
    End Sub

    Private Sub Ingresar_Click(sender As Object, e As EventArgs) Handles Ingresar.Click
        Dim conx As OleDbConnection = Conexion.ConexionBD
        Dim Cadena As String = "insert into [Linea]([CodigoCategoria],[Telefono],[CodigoCliente]) values (@cate,@tel,@clien) "
        Dim Comando As New OleDbCommand(Cadena, conx)
        Comando.Parameters.AddWithValue("@cate", ComboCategoria.SelectedValue.ToString())
        Comando.Parameters.AddWithValue("@tel", TxtTelefono.Text)
        Comando.Parameters.AddWithValue("@clien", ComboCliente.SelectedValue.ToString())
        Dim i As Int32 = Comando.ExecuteNonQuery()
        If i > 0 Then
            MessageBox.Show("Insertado con exito")
            Limpliar()
        Else
            MessageBox.Show("No se pudo Insertar")
        End If
        conx.Close()
    End Sub

    Private Sub Limpliar()
        TxtTelefono.Text = ""
    End Sub
End Class