﻿Public Class MenuPrincipal
    Private Sub Cliente_Click(sender As Object, e As EventArgs) Handles IrCiente.Click
        Dim VerCliente = New Cliente
        VerCliente.Show()
        Me.Hide()
    End Sub

    Private Sub IrCategoria_Click(sender As Object, e As EventArgs) Handles IrCategoria.Click
        Dim VerCategoria = New Categoria
        VerCategoria.Show()
        Me.Hide()

    End Sub

    Private Sub VerContratante_Click(sender As Object, e As EventArgs) Handles VerContratante.Click
        Dim VerContratante = New Contrato
        VerContratante.Show()
        Me.Hide()
    End Sub

    Private Sub VerLinea_Click(sender As Object, e As EventArgs) Handles VerLinea.Click
        Dim VerLinea = New Linea
        VerLinea.Show()
        Me.Hide()
    End Sub

    Private Sub VerReportes_Click(sender As Object, e As EventArgs) Handles VerReportes.Click
        Dim VerReportes = New Reportes
        VerReportes.Show()
        Me.Hide()

    End Sub

    Private Sub VerServicio_Click(sender As Object, e As EventArgs) Handles VerServicio.Click
        Dim VerServicio = New Servicio
        VerServicio.Show()
        Me.Hide()
    End Sub

    Private Sub ExitPrograma_Click(sender As Object, e As EventArgs) Handles ExitPrograma.Click
        Me.Close()
    End Sub
End Class
